package com.sda.hibernate;

import com.sda.hibernate.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.EntityManager;

public class SimpleCrud {
    public static void main(String[] args) {
        final SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        final EntityManager em = sessionFactory.createEntityManager();

        Employee employee = new Employee();
        employee.setEmail("jdoe@gmail.com");
        employee.setFirstName("John");
        employee.setFamilyName("Doe");

        em.getTransaction().begin();
        //CRUD here
        em.persist(employee);
        em.flush();
        Employee result = em.find(Employee.class, 1);
        System.out.println(result.getFirstName() + " " + result.getFamilyName());
        em.getTransaction().commit();

        HibernateUtils.shutdown();
    }
}
