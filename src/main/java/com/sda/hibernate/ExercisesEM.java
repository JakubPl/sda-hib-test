package com.sda.hibernate;

import com.sda.hibernate.entity.Employee;
import com.sda.hibernate.entity.Task;
import com.sda.hibernate.enums.PhoneBrand;
import org.hibernate.SessionFactory;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Scanner;

public class ExercisesEM {
    private static final SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
    private static final EntityManager entityManager = sessionFactory.createEntityManager();

    public static void main(String[] args) {
        //loading tasks pages
        /*Integer line = 0;
        while (!line.equals(-1)) {
            System.out.println("Type tasks page nr: ");
            line = new Scanner(System.in).nextInt();
            if (line != -1)
                loadTaskPage(line);
        }*/

        //filter employees by phone brand
        //filterEmployeesByPhoneBrand(PhoneBrand.SAMSUNG);

        //create project from most popular phone brand
        createProjectFromPopularPhoneBrand();

        HibernateUtils.shutdown();
    }

    public static void filterEmployeesByPhoneBrand(PhoneBrand phoneBrand) {
        List<Employee> employees = entityManager.createQuery("select e from Employee e " +
                                                                "join fetch e.phone " +
                                                                "where e.phone.phoneBrand = :brand", Employee.class)
                .setParameter("brand", phoneBrand)
                .getResultList();
        System.out.println("Users by brand: " + phoneBrand.name());
        employees.forEach(x-> System.out.println(x.getFirstName() + " " + x.getFamilyName() + " " + x.getPhone().getPhoneBrand().name()));
    }

    public static void createProjectFromPopularPhoneBrand() {
        /*int rowsInserted =entityManager.createQuery("insert into Project (project_name) " +
                "select max (cnt) from (select phone_brand, count(phone_brand) cnt from Phone group by phone_brand)")
                .executeUpdate();
        System.out.println("Projects created from phone brand: " + rowsInserted);*/
    }

    public static void deleteEmployeesWithNoTasks() {

    }

    public static void updateBlockerQueries() {

    }

    public static void loadTaskPage(int pageNr) {
        System.out.println("Tasks page nr " + pageNr);
        List<Task> results = entityManager.createQuery("from Task", Task.class)
                .setFirstResult(pageNr * 3)
                .setMaxResults(3)
                .getResultList();
        results.forEach(x -> System.out.println(x.getTitle() + " " + x.getDescription()));
    }
}
